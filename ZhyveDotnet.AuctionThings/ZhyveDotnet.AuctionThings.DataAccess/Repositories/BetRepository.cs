﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly DataContext _dataContext;

        public BetRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Bet> CreateAsync(Bet value)
        {
            await _dataContext.Set<Bet>().AddAsync(value);
            await _dataContext.SaveChangesAsync();
            return value;
        }

        public async Task DeleteAsync(Guid id)
        {
            Bet? bet = await _dataContext.Bet.SingleOrDefaultAsync(b => b.Id == id);
            if (bet == null)
            {
                throw new EntityNotFoundException($"Ставка по id {id} не найдена.");
            }

            _dataContext.Remove(bet);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<Bet>> GetAllAsync()
        {
            return await _dataContext.Bet.Where(bet => !bet.IsDeleted).ToListAsync();
        }

        public async Task<IReadOnlyList<Bet>> GetBetsByLotIdAsync(Guid lotId)
        {
            var bets = await _dataContext.Bet.Where(bet => bet.LotId == lotId).ToListAsync();
            return bets;
        }

        public async Task<IReadOnlyList<Bet>> GetBetsByUserIdAndLotIdAsync(Guid lotId, Guid userId)
        {
            return await _dataContext.Bet.Where(bet => bet.LotId == lotId && bet.UserBidderId == userId).ToListAsync();
        }

        public async Task<IReadOnlyList<Bet>> GetBetsByUserIdAsync(Guid userId)
        {
            return await _dataContext.Bet
                .Include(x => x.Lot)
                .ThenInclude(x => x!.Photos)
                .Where(bet => bet.UserBidderId == userId)
                .ToArrayAsync();
        }

        public async Task<Bet?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Bet.SingleOrDefaultAsync(bet => bet.Id == id);
        }

        public async Task UpdateAsync(Bet value)
        {
            _dataContext.Set<Bet>().Update(value);
            await _dataContext.SaveChangesAsync();
        }
    }
}