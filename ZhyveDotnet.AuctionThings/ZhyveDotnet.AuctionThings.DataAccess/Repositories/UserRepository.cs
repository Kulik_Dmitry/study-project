﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class UserRepository
        : IUserRepository
    {
        private readonly DataContext _dataContext;
        private readonly IConfiguration _configuration;

        public UserRepository(DataContext dataContext, IDateTimeProvider dateTimeProvider, IConfiguration configuration)
        {
            _dataContext = dataContext;
            _configuration = configuration;
        }

        public async Task<User> CreateAsync(User user)
        {
            await _dataContext.Set<User>().AddAsync(user);
            await _dataContext.SaveChangesAsync();
            return user;
        }

        public async Task<IReadOnlyList<User>> GetAllAsync(int pageIndex)
        {
            var pageSize = _configuration.GetValue<int>("Setting:pageSize", 20);
            return await _dataContext.Set<User>().Where(u => !u.IsDeleted).Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<IReadOnlyList<User>> GetAllAsync()
        {
            return await _dataContext.Set<User>().ToListAsync();
        }

        public async Task<User?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<User>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(Guid id)
        {
            User? user = await _dataContext.Set<User>().SingleOrDefaultAsync(x => x.Id.Equals(id));
            if (user == null)
            {
                throw new EntityNotFoundException($"Пользователь по id {id} не найден.");
            }

            _dataContext.Remove(user);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(User user)
        {
            _dataContext.Set<User>().Update(user);
            await _dataContext.SaveChangesAsync();
        }
    }
}
