﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class LotStateRepository : ILotStateRepository
    {
        private readonly DataContext _dataContext;

        public LotStateRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<LotState> CreateAsync(LotState value)
        {
            await _dataContext.Set<LotState>().AddAsync(value);
            await _dataContext.SaveChangesAsync();
            return value;
        }

        public async Task DeleteAsync(Guid id)
        {
            LotState? lotState = await _dataContext.Set<LotState>().SingleOrDefaultAsync(x => x.Id == id);
            if (lotState == null)
            {
                throw new EntityNotFoundException($"Стасут лота по id {id} не найден.");
            }

            _dataContext.Remove(lotState);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<LotState>> GetAllAsync()
        {
            return await _dataContext.Set<LotState>().Where(u => !u.IsDeleted).ToListAsync();
        }

        public async Task<LotState?> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<LotState>().SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IReadOnlyList<LotState>> GetLotStatesByLotIdAsync(Guid lotId)
        {
            return await _dataContext.Set<LotState>().Where(ls => ls.LotId == lotId).ToListAsync();
        }

        public async Task UpdateAsync(LotState value)
        {
            _dataContext.Set<LotState>().Update(value);
            await _dataContext.SaveChangesAsync();
        }
    }
}
