﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly DataContext _dataContext;

        private DbSet<Category> CategorysDbSet => _dataContext.Set<Category>();

        public CategoryRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Category> CreateAsync(Category category)
        {
            await CategorysDbSet.AddAsync(category);
            await _dataContext.SaveChangesAsync();
            return category;
        }

        public async Task DeleteAsync(Guid id)
        {
            Category? category = await CategorysDbSet.SingleOrDefaultAsync(x => x.Id == id);
            if (category == null)
            {
                throw new EntityNotFoundException($"Категория по id {id} не найдена.");
            }

            _dataContext.Remove(category);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IReadOnlyList<Category>> GetAllAsync()
        {
            return await CategorysDbSet.Where(x => !x.IsDeleted).ToListAsync();
        }

        public async Task<Category?> GetByIdAsync(Guid id)
        {
            return await CategorysDbSet.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(Category category)
        {
            CategorysDbSet.Update(category);
            await _dataContext.SaveChangesAsync();
        }
    }
}
