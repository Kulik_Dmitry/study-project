﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZhyveDotnet.AuctionThings.DataAccess.Migrations
{
    public partial class FileDocumentS : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Lots_LotId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_Comment_Users_UserAuthorId",
                table: "Comment");

            migrationBuilder.DropForeignKey(
                name: "FK_FileDocument_Lots_LotId",
                table: "FileDocument");

            migrationBuilder.DropForeignKey(
                name: "FK_Lots_FileDocument_MainPhotoId",
                table: "Lots");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FileDocument",
                table: "FileDocument");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comment",
                table: "Comment");

            migrationBuilder.RenameTable(
                name: "FileDocument",
                newName: "FileDocuments");

            migrationBuilder.RenameTable(
                name: "Comment",
                newName: "Comments");

            migrationBuilder.RenameIndex(
                name: "IX_FileDocument_LotId",
                table: "FileDocuments",
                newName: "IX_FileDocuments_LotId");

            migrationBuilder.RenameIndex(
                name: "IX_Comment_UserAuthorId",
                table: "Comments",
                newName: "IX_Comments_UserAuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Comment_LotId",
                table: "Comments",
                newName: "IX_Comments_LotId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Lots",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PublicName",
                table: "FileDocuments",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PrivateKey",
                table: "FileDocuments",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "LotId",
                table: "FileDocuments",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "LocalName",
                table: "FileDocuments",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FileDocuments",
                table: "FileDocuments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comments",
                table: "Comments",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Lots_LotId",
                table: "Comments",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_UserAuthorId",
                table: "Comments",
                column: "UserAuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FileDocuments_Lots_LotId",
                table: "FileDocuments",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_FileDocuments_MainPhotoId",
                table: "Lots",
                column: "MainPhotoId",
                principalTable: "FileDocuments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Lots_LotId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_UserAuthorId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_FileDocuments_Lots_LotId",
                table: "FileDocuments");

            migrationBuilder.DropForeignKey(
                name: "FK_Lots_FileDocuments_MainPhotoId",
                table: "Lots");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FileDocuments",
                table: "FileDocuments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comments",
                table: "Comments");

            migrationBuilder.RenameTable(
                name: "FileDocuments",
                newName: "FileDocument");

            migrationBuilder.RenameTable(
                name: "Comments",
                newName: "Comment");

            migrationBuilder.RenameIndex(
                name: "IX_FileDocuments_LotId",
                table: "FileDocument",
                newName: "IX_FileDocument_LotId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_UserAuthorId",
                table: "Comment",
                newName: "IX_Comment_UserAuthorId");

            migrationBuilder.RenameIndex(
                name: "IX_Comments_LotId",
                table: "Comment",
                newName: "IX_Comment_LotId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Lots",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PublicName",
                table: "FileDocument",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PrivateKey",
                table: "FileDocument",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<Guid>(
                name: "LotId",
                table: "FileDocument",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LocalName",
                table: "FileDocument",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_FileDocument",
                table: "FileDocument",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comment",
                table: "Comment",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Lots_LotId",
                table: "Comment",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comment_Users_UserAuthorId",
                table: "Comment",
                column: "UserAuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FileDocument_Lots_LotId",
                table: "FileDocument",
                column: "LotId",
                principalTable: "Lots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Lots_FileDocument_MainPhotoId",
                table: "Lots",
                column: "MainPhotoId",
                principalTable: "FileDocument",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
