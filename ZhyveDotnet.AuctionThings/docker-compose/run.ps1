# $env:USERPROFILE works only in windows and means %USERPROFILE%
# https://devblogs.microsoft.com/scripting/powertip-use-powershell-to-find-user-profile-path/

# https://docs.microsoft.com/en-us/aspnet/core/security/docker-compose-https?view=aspnetcore-3.1
dotnet dev-certs https -ep %USERPROFILE%\.aspnet\https\aspnetapp.pfx -p 12qwasZX
dotnet dev-certs https --trust

docker-compose -f "docker-compose.yml" stop
docker-compose -f "docker-compose.yml" rm --force
docker-compose -f "docker-compose.yml" up 