﻿import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card } from 'react-bootstrap'
import { currencyConverter } from '../components/functions/currencyConverter';
import CloseOrBet from '../components/Elements/CloseOrBet';

class LotDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state.id,
            name: null,
            currency: 0,
            description: null,
            initialCost: 0,
            userOwner: null,
            userId: null,
            leadingBetAmount: 0
        };

    }

    async componentDidMount() {
		fetch('http://localhost:7000/api/v1/Lot/' + this.state.id)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
				let currency = currencyConverter(data.currency);
				let amount = data.leadingBet === null ? 0 : data.leadingBet.amount;
                this.setState({
                    ...this.state, ...{
                        name: data.name,
                        currency: currency,
                        description: data.description,
                        initialCost: data.initialCost,
                        userOwner: data.userOwner.login,
                        userId: data.userOwner.id,
						leadingBetAmount: amount
                    }
                });

            });
    }

    render() {
        return (
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <Card className="bg-dark card">
                            <Card.Header>{this.state.name}</Card.Header>
                            <Card.Img
                                variant="top"
                                //TODO GET REAL IMAGE!!!!
                                src="https://upload.wikimedia.org/wikipedia/commons/7/71/Federico_Cervelli_-_Lot_and_his_daughters_-_Asta_Sotheby.jpg"/>
                            
                            <Card.Body className="p-2">
                                <Card.Text align="left">
                                    <p>Начальная цена: {this.state.initialCost} {this.state.currency}</p>
                                    <p>Ведущая ставка: {this.state.leadingBetAmount} {this.state.currency}</p>
                                </Card.Text>
                            </Card.Body>
                            <Card.Footer>
                                <Card.Text align="left">
                                    <p>Владелец лота: {this.state.userOwner}</p>
                                </Card.Text>
                            </Card.Footer>
                        </Card>
                    </div>
                    <div class="col-7">
                        <p align="left">Описание лота:</p>
                        <p align="left">{this.state.description}</p>
                        <CloseOrBet
                            lotId={this.state.id}
                            userId={this.state.userId}
                            //isMine={false} />
                            isMine={true} />
                    </div>
                </div>
            </div>
        )
    }
}

export default LotDetails;