import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card } from 'react-bootstrap'
import { LotDetailsButton } from "../components/Elements/LotDetailsButton";
import CloseOrBet from '../components/Elements/CloseOrBet';

class LotCardsView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            lots: []
        };

        this.getMax = this.getMax.bind(this);
    }

    getMax(arr, prop) {
        var max;
        if (arr)
        {
            for (var i=0 ; i<arr?.length ; i++) {
                if (max == null || parseFloat(arr[i][prop]) > parseFloat(max[prop]))
                    max = arr[i];
            }
            return max.amount;
        }
        else return max;
     }

    async Refresh() {

        //TODO GET REAL ID!!!!
		let userId = "171129af-a968-4cbf-9acc-edaad8a740fc";
		fetch('http://localhost:7000/api/v1/Lot')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        lots: result.reduce(function(result, item) {
                            //фильтрация
                            if (item.currentState.status == 3) {
                              result.push(item);
                            }
                            else if (item.userOwnerId == userId) {
                                result.push(item);
                            }
                            return result;
                          }, [])
                        .map(function (item) {
                            const currentBet = item.bets.length > 0 ? this.getMax(item.bets, "amount") : 0;
                            return {
                                id: item.id,
                                name: item.name,
                                description: item.description,
                                currentBet: currentBet ?? item.InitialCost,
                                isMine: item.userOwnerId === userId
                            }
                        },this)
                    });
                },
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    componentDidMount() {
        this.Refresh()
    }

    render() {
        const { error, lots } = this.state;

        if (error) {
            return (
                <div>Ошибка: {error.message}</div>
            )
        } else {
            return (
                <div>
                <button  className='btn btn-primary' style={{margin: '20px'}} onClick={()=>this.Refresh()}>Обновить</button>
                <div className="container-fluid">
                    <div className="row justify-content-center">
                        {lots.map(item => (
                            <Lot
                                //isMine={item.isMine}
                                id = {item.id}
                                header = {item.name}
                                currentCost = {'Текущая цена:' +  item.currentBet}
                                shortDescription = {item.ShortDescription}
                                dateCreated = {item.dateCreated}
                                //TODO CHECK IF MINE BY USING ID and OwnerID!!!!
                                isMine = {false}
                            />
                        ))}
                    </div>
                </div>
                </div>
            )
        }
    }
}

const Lot = (props) => {

    let lotId = props.id;
    //TODO GET REAL ID!!!!
    let userId = "171129af-a968-4cbf-9acc-edaad8a740fc";
    //let isMine = props.isMine;

    return (
        <Card className="bg-dark card col-lg-3 m-2 p-2">
            <Card.Header>{props.header}</Card.Header>
            <Card.Img
                variant="top"
                //TODO GET REAL IMAGE!!!!
                src="https://upload.wikimedia.org/wikipedia/commons/7/71/Federico_Cervelli_-_Lot_and_his_daughters_-_Asta_Sotheby.jpg"/>            
            <Card.Body className="p-2">
                <Card.Title className="text-left">{props.currentCost}</Card.Title>
                <Card.Text>{props.shortDescription}</Card.Text>
            </Card.Body>
            <Card.Footer>
                <div className="align-items-baseline justify-content-around">
                    <CloseOrBet
                        lotId={lotId}
                        userId={userId}
                        //isMine={false} />
                        isMine={false} />
                    <LotDetailsButton id={lotId} />
                </div>
            </Card.Footer>
        </Card>
    )
}

export default LotCardsView;

