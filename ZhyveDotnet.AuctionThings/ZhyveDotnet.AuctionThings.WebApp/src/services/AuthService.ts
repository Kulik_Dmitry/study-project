import { Log, User, UserManager } from 'oidc-client';

import { Constants } from '../helpers/Constants';

export class AuthService {
  public userManager: UserManager;

  constructor() {
    const settings = {
        authority: "http://localhost:5001/",
        client_id: "js_oidc",
        redirect_uri: "http://localhost:3000/callback.html",
        silent_redirect_uri: "http://localhost:3000/silent.html",
        post_logout_redirect_uri: "http://localhost:3000/index.html",
        scope: "openid profile email api1",
        response_type: "id_token token",

      
    };
    this.userManager = new UserManager(settings);

    Log.logger = console;
    Log.level = Log.INFO;
  }

  public getUser(): Promise<User | null> {
    return this.userManager.getUser();
  }

  public login(): Promise<void> {
    return this.userManager.signinRedirect();
  }

  public renewToken(): Promise<User> {
    return this.userManager.signinSilent();
  }

  public logout(): Promise<void> {
    return this.userManager.signoutRedirect();
  }
}
