export class Constants {
    public static stsAuthority = 'https://demo.identityserver.io/';
    public static clientId = 'js_oidc';
    public static clientRoot = 'http://localhost:3000/';
    public static clientScope = 'openid profile email api1';

    public static apiRoot = 'http://localhost:5000/api/v1/'
}
