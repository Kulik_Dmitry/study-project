import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { renderRoutes } from "react-router-config";
import { ModalProvider } from './context/modal';
import { ModalRoot } from './components/Modal';
import { routes } from './routes';
import { ToastContainer, toast } from 'react-toastify'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    return (
        <div className="App">
            <ModalProvider>
                <header className="App-header">
                    {renderRoutes(routes)}
                    <ModalRoot />
                </header>
            </ModalProvider>
            <ToastContainer />
        </div>
    );
}

export default App;
