﻿import React from 'react';

const CloseOrBet = (props) => {
    let userId = props.userId;
    let lotId = props.lotId;
    let isMine = props.isMine;

    let textInput = React.createRef();

    async function MakeBet(userId, lotId) {
        console.log(userId);
        console.log(lotId);
		let response = await fetch(`http://localhost:7000/api/v1/Bet?userId=${userId}&lotId=${lotId}&amount=${textInput.current.value}`, { method: 'POST' });
        if (!response.ok) {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    async function CloseLot(lotId) {
		let response = await fetch(`http://localhost:7000/api/v1/Lot/${lotId}`, { method: 'PUT' });
        if (!response.ok) {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    let element;

    if (isMine) {
        element = <button className='btn btn-primary' style={{ margin: '20px' }} onClick={() => CloseLot(lotId)}>Закрыть торги</button>
    } else {
        element =
            <div className="d-flex">
                <input ref={textInput} className="form-control" style={{ width: '100%', margin: 'auto' }} placeholder="Ваша ставка" />
            <button className='btn btn-primary' style={{ margin: '20px' }} onClick={() => MakeBet(userId, lotId)}>Ставка</button>
            </div>
    }
    return (element);
}

export default CloseOrBet;