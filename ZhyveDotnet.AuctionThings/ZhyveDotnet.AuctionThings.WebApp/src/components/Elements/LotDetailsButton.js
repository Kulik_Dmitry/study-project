﻿import { useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

export function LotDetailsButton(id) {
    let history = useHistory();

    function handleClick() {
        history.push("/LotDetails", id);        
    }

    return (
        <button type="button" className='btn btn-primary' onClick={handleClick}>
            Детали
        </button>
    );
}