﻿import React from 'react';
import { BaseModal } from './BaseModal';
import { Button, Form, Container } from 'react-bootstrap'
import { FaGoogle, FaFacebookF } from 'react-icons/fa';

export const Login = ({ onRequestClose, ...otherProps }) => (
    <BaseModal onRequestClose={onRequestClose} {...otherProps}>
        <Container>
            <h1>Здравствуйте!</h1>
            <br></br>
            <Form.Control type="text" placeholder="Введите логин" />
            <br></br>
            <Form.Control type="password" placeholder="Введите пароль" />
            <br></br>
            <Button size="small" variant="success" onClick={onRequestClose}>Войти</Button>
            <br></br>

            <Button variant="outline-info"><FaGoogle/> Продолжить с Google</Button>
            <br></br>
            <Button variant="outline-primary"><FaFacebookF />Продолжить с Facebook</Button>
        </Container>
    </BaseModal>
);
