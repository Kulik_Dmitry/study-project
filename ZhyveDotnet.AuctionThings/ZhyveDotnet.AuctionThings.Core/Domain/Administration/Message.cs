﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Message : BaseEntity
    {
        public virtual User? UserSender { get; set; }

        public Guid UserSenderId { get; set; }

        public virtual User? UserRecipient { get; set; }

        public Guid UserRecipientId { get; set; }

        public string? Content { get; set; }

        protected Message()
        {
        }
    }
}