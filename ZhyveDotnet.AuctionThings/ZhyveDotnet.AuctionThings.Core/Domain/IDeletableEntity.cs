﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public interface IDeletableEntity
    {
        public bool IsDeleted { get; set; }
    }
}
