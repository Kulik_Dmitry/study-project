﻿using System;
using System.IO;
using System.Web;

namespace ZhyveDotnet.AuctionThings.Core.Domain.Files
{
    /// <summary>
    /// Файл [System.IO.File занято].
    /// </summary>
    public class FileDocument : BaseEntity
    {
        /// <summary>
        /// Gets or sets публичное имя файла [матрешка.jpg].
        /// </summary>
        public string PublicName { get; protected set; }

        /// <summary>
        /// Gets or sets внутреннее имя файла [b0d4ce5d-2757-4699-948c-cfa72ba94f86.pdf - и это не Id].
        /// </summary>
        public string LocalName { get; protected set; }

        /// <summary>
        /// Gets or sets ключ доступа к файлу [у каждого свой уникальный, чтобы прямые ссылки на файлы можно было при необходимости закрывать сбросом ключа].
        /// </summary>
        public string PrivateKey { get; protected set; }

        public Guid? LotId { get; protected set; }

        public virtual Lot? Lot { get; protected set; }

        /// <summary>
        /// Id пользователя-владельца.
        /// </summary>
        public Guid? UserId { get; protected set; }

        /// <summary>
        /// Пользователь-владелец.
        /// </summary>
        public User? User { get; protected set; }

        /// <summary>
        /// Конструктор для Entity Framework.
        /// </summary>
        protected FileDocument()
        {
            this.PublicName = string.Empty;
            this.LocalName = string.Empty;
            this.PrivateKey = string.Empty;
        }

        public FileDocument(string fileName, Guid userId)
            : this()
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException(nameof(fileName));
            }

            this.PublicName = Path.GetFileName(HttpUtility.UrlDecode(fileName));

            this.SetLocalName()
                .SetPrivateKey();

            UserId = userId;
        }

        /// <summary>
        /// Загрузить новый файл по старой ссылке.
        /// </summary>
        /// <param name="fileNameNew">Имя нового файла.</param>
        /// <returns>Файл.</returns>
        public FileDocument Update(string fileNameNew)
        {
            this.Rename(fileNameNew)
                .SetLocalName()
                .SetPrivateKey();

            return this;
        }

        /// <summary>
        /// Переименовать файл.
        /// </summary>
        /// <param name="fileNameNew">Новое имя файла.</param>
        /// <returns>Файл.</returns>
        /// <exception cref="ArgumentException">Пустое имя файла.</exception>
        public FileDocument Rename(string fileNameNew)
        {
            if (string.IsNullOrEmpty(fileNameNew))
            {
                throw new ArgumentException(nameof(fileNameNew));
            }

            this.PublicName = Path.GetFileName(fileNameNew);

            return this;
        }

        /// <summary>
        /// Установить ключ доступа к файлу.
        /// </summary>
        /// <returns>Файл.</returns>
        public FileDocument SetPrivateKey()
        {
            // TODO: Временное решение, пока не написан свой криптопровайдер
            this.PrivateKey = Guid.NewGuid().ToString();

            return this;
        }

        /// <summary>
        /// Установить внутреннее имя файла.
        /// </summary>
        /// <returns>Файл.</returns>
        private FileDocument SetLocalName()
        {
            this.LocalName = Guid.NewGuid().ToString() + Path.GetExtension(this.PublicName);

            return this;
        }
    }
}