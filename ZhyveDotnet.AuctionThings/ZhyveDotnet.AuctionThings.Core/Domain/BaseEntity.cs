﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    /// <summary>
    /// Основная сущность.
    /// </summary>
    public class BaseEntity : IDeletableEntity, IDateEntity
    {
        /// <summary>
        /// Gets or sets id сущности.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets дата создания сущности.
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets or sets дата обновления сущности.
        /// </summary>
        public DateTime DateUpdated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether признак удаленности сущности.
        /// </summary>
        public bool IsDeleted { get; set; }

        public BaseEntity()
        {
            this.Id = Guid.NewGuid();

            var now = DateTime.Now;
            this.DateCreated = now;
            this.DateUpdated = now;
        }
    }
}
