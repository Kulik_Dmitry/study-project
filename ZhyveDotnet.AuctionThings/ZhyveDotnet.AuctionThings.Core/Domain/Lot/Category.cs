﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Category : BaseEntity
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public Guid? ParentId { get; set; }

        protected Category()
        {
        }

        public Category(string name, string? description, Guid? parentId)
        {
            Name = name;

            Description = description;

            ParentId = parentId;
        }
    }
}