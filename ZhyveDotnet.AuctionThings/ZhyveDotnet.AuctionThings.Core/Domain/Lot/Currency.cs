﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public enum Currency
    {
        /// <summary>
        /// Не заданно
        /// </summary>
        NotSet,

        /// <summary>
        /// Американский доллар
        /// </summary>
        USD,

        /// <summary>
        /// Евро
        /// </summary>
        EUR,

        /// <summary>
        /// Белорусский рубль
        /// </summary>
        BYN,

        /// <summary>
        /// Российский рубль
        /// </summary>
        RUB,

        /// <summary>
        /// Гривна
        /// </summary>
        UAH,

        /// <summary>
        /// Казахстанский тенге
        /// </summary>
        KZT
    }
}
