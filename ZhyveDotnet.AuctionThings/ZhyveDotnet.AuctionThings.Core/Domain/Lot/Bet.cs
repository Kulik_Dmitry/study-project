﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Bet : BaseEntity
    {
        public Guid UserBidderId { get; protected set; }

        public virtual User? UserBidder { get; protected set; }

        public decimal Amount { get; protected set; }

        public Guid LotId { get; protected set; }

        public virtual Lot? Lot { get; protected set; }

        public bool IsWinner { get; protected set; }

        protected Bet()
        {
        }

        public Bet(Guid userId, Guid lotId, decimal amount)
            : base()
        {
            UserBidderId = userId;
            LotId = lotId;
            Amount = amount;
        }

        public void AssingAsWinner() => IsWinner = true;
    }
}
