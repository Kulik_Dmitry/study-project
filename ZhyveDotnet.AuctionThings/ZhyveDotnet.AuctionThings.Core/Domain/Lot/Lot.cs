﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.Core.Domain
{
    public class Lot : BaseEntity
    {
        public string Name { get; protected set; }

        public string? Description { get; protected set; }

        public string? ShortDescription { get; protected set; }

        public Guid UserOwnerId { get; protected set; }

        [NotMapped]
        public bool IsMine { get; set; }

        public virtual User? UserOwner { get; protected set; }

        [Column(TypeName = "varchar(24)")]
        public Currency Currency { get; protected set; }

        public decimal InitialCost { get; protected set; }

        public virtual IList<Category>? Categories { get; protected set; }

        public virtual IList<LotState> States { get; protected set; }

        [NotMapped]
        public LotState? CurrentState => States.OrderBy(s => s.Status).LastOrDefault();

        public virtual IList<Bet>? Bets { get; protected set; }

        [NotMapped]
        public Bet? WinnerBet => Bets?.FirstOrDefault(bet => bet.IsWinner);

        [NotMapped]
        public Bet? LeadingBet { get; set; }

        public virtual IList<Comment>? Comments { get; protected set; }

        [NotMapped]
        public FileDocument? MainPhoto => Photos?.FirstOrDefault();

        public virtual IList<FileDocument>? Photos { get; protected set; }

        protected Lot()
        {
            Name = string.Empty;
            States = new List<LotState>();
        }

        public Lot(string name, decimal initialCost, Guid userOwnerId, Currency currency)
            : base()
        {
            Name = name;
            InitialCost = initialCost;
            UserOwnerId = userOwnerId;
            Currency = currency;
            States = new List<LotState>();
            States.Add(new LotState(Id, StatusLot.Draft));
        }

        public Lot(LotCreateDto dto)
            : this(dto.Name, dto.InitialCost, dto.UserOwnerId, dto.Currency)
        {
            Description = dto.Description;
            ShortDescription = dto.ShortDescription;
            Categories = dto.Categories;
            Photos = dto.Photos;
        }

        public Lot Update(LotUpdateDto dto)
        {
            Name = string.IsNullOrWhiteSpace(dto.Name) ? Name : dto.Name;
            Description = string.IsNullOrWhiteSpace(dto.Description) ? Description : dto.Description;
            ShortDescription = string.IsNullOrWhiteSpace(dto.ShortDescription) ? ShortDescription : dto.ShortDescription;
            Categories = (dto.Categories == null) ? Categories : dto.Categories;
            Photos = (dto.Photos == null) ? Photos : dto.Photos;
            return this;
        }

        internal void Close()
        {
            States.Add(new LotState(Id, StatusLot.Completed));
        }
    }
}
