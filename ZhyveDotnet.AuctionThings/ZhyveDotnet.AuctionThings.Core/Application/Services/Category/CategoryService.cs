﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ILotRepository _lotRepository;

        public CategoryService(ICategoryRepository repository, ILotRepository lotRepository)
        {
            _categoryRepository = repository;
            _lotRepository = lotRepository;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await _categoryRepository.GetAllAsync();
        }

        public async Task<Category?> GetCategoryOrNullAsync(Guid id)
        {
            Category? category = await _categoryRepository.GetByIdAsync(id);
            return category;
        }

        public async Task DeleteAsync(Guid id)
        {
            var lots = await _lotRepository.GetAllAsync();

            // Нельзя удалять категории если есть неудаленные лоты принадлежащие к этой категорию
            if (lots.Where(lot => lot.Categories != null && lot.Categories.Any(category => category.Id == id) && lot.IsDeleted == false).Any())
            {
                throw new InvalidOperationException("Категория не может быть удалена");
            }

            await _categoryRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(Guid id, CategoryDto dto)
        {
            if (!dto.IsValid())
            {
                throw new ArgumentException("Невалидные данные");
            }

            Category? category = await _categoryRepository.GetByIdAsync(id);

            if (category == null)
            {
                throw new EntityNotFoundException(id);
            }

            var nameExists = await CategoryNameExistsAsync(dto.Name);

            if (nameExists)
            {
                throw new ArgumentException("Категория с таким именем уже существует");
            }

            category.Name = string.IsNullOrEmpty(dto.Name) ? category.Name : dto.Name;
            category.Description = string.IsNullOrEmpty(dto.Description) ? category.Description : dto.Description;

            await _categoryRepository.UpdateAsync(category);
        }

        public async Task<Category> CreateAsync(CategoryDto dto)
        {
            var nameExists = await CategoryNameExistsAsync(dto.Name);

            if (string.IsNullOrEmpty(dto.Name))
            {
                throw new ArgumentException("Невалидные данные");
            }

            if (nameExists)
            {
                throw new ArgumentException("Категория с таким именем уже существует");
            }

            Category category = new Category(dto.Name, dto.Description, null);

            return await _categoryRepository.CreateAsync(category);
        }

        private async Task<bool> CategoryNameExistsAsync(string? name)
        {
            var categories = await _categoryRepository.GetAllAsync();

            return categories.Where(category => category.Name == name).Any();
        }
    }
}
