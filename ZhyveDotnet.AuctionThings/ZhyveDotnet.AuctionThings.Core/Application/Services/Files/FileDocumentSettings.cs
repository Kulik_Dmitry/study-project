﻿namespace ZhyveDotnet.AuctionThings.Core.Application.Services.Files
{
    /// <summary>
    /// Файловые настройки.
    /// </summary>
    public class FileDocumentSettings
    {
        /// <summary>
        /// Gets or sets путь файлового хранилища.
        /// </summary>
        public string StoragePath { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets количество попыток для загрузки.
        /// </summary>
        public int CountUploadTries { get; set; }
    }
}