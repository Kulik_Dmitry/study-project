﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.Comment;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services.Comment
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public async Task<Domain.Comment> CreateAsync(CommentDto dto)
        {
            if (!dto.IsValidForCreate())
            {
                throw new ArgumentException("Невалидные данные");
            }

            Domain.Comment comment = new Domain.Comment(dto.UserAuthorId, dto.LotId, dto.Content);
            return await _commentRepository.CreateAsync(comment);
        }

        public async Task DeleteAsync(Guid lot)
        {
            await _commentRepository.DeleteAsync(lot);
        }

        public async Task<IEnumerable<Domain.Comment>> GetAllByLotIdAsync(Guid lotId)
        {
            return await _commentRepository.GetAllByLotIdAsync(lotId);
        }

        public async Task UpdateAsync(Guid id, string content)
        {
            var comment = await _commentRepository.GetByIdAsync(id);
            if (comment == null)
            {
                throw new EntityNotFoundException(id);
            }

            comment = comment.Update(content);
            await _commentRepository.UpdateAsync(comment);
        }
    }
}
