﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services.Role
{
    public class RoleService
        : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task CreateAsync(Domain.Role role)
        {
            await _roleRepository.CreateAsync(role);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _roleRepository.DeleteAsync(id);
        }

        public async Task<IEnumerable<Domain.Role>> GetAllAsync()
        {
            return await _roleRepository.GetAllAsync();
        }

        public async Task<Domain.Role?> GetLotOrNullAsync(Guid id)
        {
            Domain.Role? role = await _roleRepository.GetByIdAsync(id);
            return role;
        }

        public async Task UpdateAsync(Guid id, Domain.Role role)
        {
            if (await _roleRepository.GetByIdAsync(id) == null)
            {
                throw new EntityNotFoundException(id);
            }

            await _roleRepository.UpdateAsync(role);
        }
    }
}
