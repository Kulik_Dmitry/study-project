﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services
{
    public class LotStateService : ILotStateService
    {
        private readonly ILotStateRepository lotStateRepository;

        public LotStateService(ILotStateRepository lotStateRepository)
        {
            this.lotStateRepository = lotStateRepository;
        }

        public async Task<Lot> AddNewStatusInLotAsync(Lot lot, StatusLot statusLot)
        {
            LotState lotState = new LotState(lot.Id, statusLot);
            await lotStateRepository.CreateAsync(lotState);
            return lot;
        }

        public async Task DeleteAsync(Guid id)
        {
            await lotStateRepository.DeleteAsync(id);
        }

        public async Task<LotState?> GetLotStateOrNullAsync(Guid id)
        {
            return await lotStateRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<LotState>> GetLotStatesByLotIdAsync(Guid lotId)
        {
            return await lotStateRepository.GetLotStatesByLotIdAsync(lotId);
        }
    }
}
