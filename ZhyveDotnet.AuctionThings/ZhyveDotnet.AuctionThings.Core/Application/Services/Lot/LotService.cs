﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Application.Validators;
using ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Services
{
    public class LotService : ILotService
    {
        private readonly ILotRepository _lotRepository;
        private readonly IValidator<Lot> _lotValidator;
        private readonly IWinningStrategy _winningStrategy;

        public LotService(ILotRepository lotRepository, IValidator<Lot> lotValidator, IWinningStrategy winningStrategy)
        {
            _lotRepository = lotRepository;
            _lotValidator = lotValidator;
            _winningStrategy = winningStrategy;
        }

        public async Task<IEnumerable<Lot>> GetAllAsync()
        {
            var lots = await _lotRepository.GetAllAsync();
            foreach (var lot in lots)
            {
                DetermineOwner(lot);
            }

            return lots;
        }

        public async Task<Lot?> GetLotOrNullAsync(Guid id)
        {
            Lot? lot = await _lotRepository.GetByIdAsync(id);
            if (lot != null)
            {
                DetermineOwner(lot);
            }

            if (lot?.Bets != null && lot.Bets.Count > 0)
            {
                lot.LeadingBet = _winningStrategy.Determine(lot.Bets);
            }

            return lot;
        }

        public async Task DeleteAsync(Guid id)
        {
            await _lotRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(Guid id, LotUpdateDto dto)
        {
            Lot? lot = await _lotRepository.GetByIdAsync(id);
            if (lot == null)
            {
                throw new EntityNotFoundException(id);
            }

            lot = lot.Update(dto);
            await _lotRepository.UpdateAsync(lot);
        }

        public async Task<Guid> CreateAsync(LotCreateDto dto)
        {
            Lot lot = new Lot(dto);
            _lotValidator.ValidateAndThrow(lot);
            await _lotRepository.CreateAsync(lot);
            return lot.Id;
        }

        private Lot DetermineOwner(Lot lot)
        {
            // TODO добавить получение guid текущего пользователя
            Guid currentUserId = new Guid("171129af-a968-4cbf-9acc-edaad8a740fc");
            lot.IsMine = lot.UserOwnerId == currentUserId;
            return lot;
        }
    }
}
