﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Validators
{
    public class BetValidator : AbstractValidator<Bet>
    {
        private readonly ILotRepository lotRepository;
        private readonly IUserRepository userRepository;

        public BetValidator(ILotRepository lotRepository, IUserRepository userRepository)
        {
            this.lotRepository = lotRepository;
            this.userRepository = userRepository;
            RuleFor(bet => bet.Amount)
                .Must(amount => amount > 0)
                .WithMessage("Сумма ставки должна быть больше 0!");

            RuleFor(bet => bet.LotId)
                .NotEmpty()
                .WithMessage("Id лота не может быть пустым!")
                .MustAsync((lotId, token) => LotIsAvailableAsync(lotId))
                .WithMessage("Указанный лот отсутствует или был удален!");

            RuleFor(bet => bet.UserBidderId)
                .NotEmpty()
                .WithMessage("Необходимо указать владельца лота!")
                .MustAsync((userId, token) => UserIsAvailableAsync(userId))
                .WithMessage("По указанному Id пользователь отсутствует либо был удален!");

            RuleFor(bet => bet)
                .MustAsync((bet, token) => BetIsMoreThanInitialCostAsync(bet))
                .WithMessage("Невозможно совершить ставку, так как ставка не превышает минимальное значение, установленное владельцем лота.");
        }

        private async Task<bool> BetIsMoreThanInitialCostAsync(Bet bet)
        {
            var lot = await lotRepository.GetByIdAsync(bet.LotId);
            return lot != null && bet.Amount >= lot.InitialCost;
        }

        private async Task<bool> UserIsAvailableAsync(Guid userId)
        {
            var user = await userRepository.GetByIdAsync(userId);
            return user != null && !user.IsDeleted;
        }

        private async Task<bool> LotIsAvailableAsync(Guid lotId)
        {
            var lot = await lotRepository.GetByIdAsync(lotId);
            return lot != null && !lot.IsDeleted;
        }
    }
}
