﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.Lot;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto.Comment
{
    public class CommentDto
    {
        public Guid UserAuthorId { get; set; }

        public string? Content { get; set; }

        public Guid LotId { get; set; }

        public bool IsValidForCreate()
        {
            bool result = true;

            if (string.IsNullOrEmpty(Content) || UserAuthorId == Guid.Empty || LotId == Guid.Empty)
            {
                result = false;
            }

            return result;
        }
    }
}
