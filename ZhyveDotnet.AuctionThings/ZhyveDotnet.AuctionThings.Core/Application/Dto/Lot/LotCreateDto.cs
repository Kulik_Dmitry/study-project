﻿using System;
using System.Collections.Generic;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto
{
    public class LotCreateDto : LotUpdateDto
    {
        public new string Name { get; set; } = string.Empty;

        public Guid UserOwnerId { get; set; }

        public decimal InitialCost { get; set; }

        public Currency Currency { get; set; }

        public IReadOnlyList<Guid>? FileIds { get; set; }
    }
}