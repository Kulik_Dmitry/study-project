﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto
{
    public class LotUpdateDto
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

        public string? ShortDescription { get; set; }

        public IList<Category>? Categories { get; set; }

        public Guid? MainPhotoId { get; set; }

        public IList<FileDocument>? Photos { get; set; }
    }
}
