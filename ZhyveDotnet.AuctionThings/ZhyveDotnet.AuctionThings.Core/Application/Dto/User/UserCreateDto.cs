﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Dto.User
{
    public class UserCreateDto
    {
        public string? Login { get; set; }

        public string? Password { get; set; }

        public string? Phone { get; set; }
    }
}
