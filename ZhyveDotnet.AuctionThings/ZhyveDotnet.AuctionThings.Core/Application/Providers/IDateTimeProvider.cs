﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Providers
{
    public interface IDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
