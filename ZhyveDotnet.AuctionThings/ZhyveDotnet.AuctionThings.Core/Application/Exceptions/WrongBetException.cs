﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZhyveDotnet.AuctionThings.Core.Application.Exceptions
{
    public class WrongBetException
        : Exception
    {
        public WrongBetException()
        {
        }

        public WrongBetException(string message)
            : base(message)
        {
        }

        public WrongBetException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
