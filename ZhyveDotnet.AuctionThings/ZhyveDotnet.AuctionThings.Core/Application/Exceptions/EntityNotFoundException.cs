﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Application.Exceptions
{
    public class EntityNotFoundException
        : Exception
    {
        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(Guid entityId)
            : base($"Сущность с id {entityId} не найдена")
        {
        }

        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        public EntityNotFoundException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}