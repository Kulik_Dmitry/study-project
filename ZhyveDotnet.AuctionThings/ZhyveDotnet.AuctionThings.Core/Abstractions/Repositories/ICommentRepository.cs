﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface ICommentRepository : IRepository<Comment>
    {
        Task<IReadOnlyList<Comment>> GetAllByLotIdAsync(Guid lotId);
    }
}
