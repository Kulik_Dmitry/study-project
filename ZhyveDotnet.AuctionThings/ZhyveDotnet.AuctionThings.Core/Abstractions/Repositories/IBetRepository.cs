﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface IBetRepository : IRepository<Bet>
    {
        Task<IReadOnlyList<Bet>> GetBetsByLotIdAsync(Guid lotId);

        Task<IReadOnlyList<Bet>> GetBetsByUserIdAsync(Guid userId);

        Task<IReadOnlyList<Bet>> GetBetsByUserIdAndLotIdAsync(Guid lotId, Guid userId);
    }
}
