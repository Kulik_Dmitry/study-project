﻿using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories
{
    public interface ILotRepository : IRepository<Lot>
    {
    }
}
