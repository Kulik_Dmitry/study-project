﻿using System;
using System.Collections.Generic;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Bases;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files
{
    public class FileDocumentFilter : BaseFilter
    {
        public IReadOnlyCollection<Guid?>? LotIds { get; set; }

        public string? PartOfFileName { get; set; }
    }
}