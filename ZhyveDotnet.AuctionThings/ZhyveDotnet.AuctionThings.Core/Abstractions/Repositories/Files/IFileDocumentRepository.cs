﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files
{
    public interface IFileDocumentRepository : IRepository<FileDocument>
    {
        Task<FileDocument?> GetItemAsync(FileDocumentFilter? filter);

        Task<IReadOnlyList<FileDocument>> GetCollectionAsync(FileDocumentFilter? filter);
    }
}