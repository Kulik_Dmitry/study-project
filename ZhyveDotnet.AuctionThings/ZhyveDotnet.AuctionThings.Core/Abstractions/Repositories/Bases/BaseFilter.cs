﻿using System;
using System.Collections.Generic;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Bases
{
    public class BaseFilter
    {
        public IReadOnlyCollection<Guid>? Ids { get; set; }

        public bool? IsDeleted { get; set; }
    }
}