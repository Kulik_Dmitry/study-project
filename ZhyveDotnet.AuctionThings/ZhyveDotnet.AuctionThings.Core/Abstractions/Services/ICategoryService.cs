﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> GetAllAsync();

        Task<Category?> GetCategoryOrNullAsync(Guid id);

        Task<Category> CreateAsync(CategoryDto dto);

        Task UpdateAsync(Guid id, CategoryDto dto);

        Task DeleteAsync(Guid lot);
    }
}
