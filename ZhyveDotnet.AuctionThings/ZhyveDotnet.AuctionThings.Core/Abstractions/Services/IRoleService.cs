﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface IRoleService
    {
        Task<IEnumerable<Role>> GetAllAsync();

        Task<Role?> GetLotOrNullAsync(Guid id);

        Task CreateAsync(Role role);

        Task UpdateAsync(Guid id, Role role);

        Task DeleteAsync(Guid id);
    }
}
