﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.Comment;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface ICommentService
    {
        Task<IEnumerable<Comment>> GetAllByLotIdAsync(Guid lotId);

        Task<Comment> CreateAsync(CommentDto dto);

        Task UpdateAsync(Guid id, string content);

        Task DeleteAsync(Guid lot);
    }
}
