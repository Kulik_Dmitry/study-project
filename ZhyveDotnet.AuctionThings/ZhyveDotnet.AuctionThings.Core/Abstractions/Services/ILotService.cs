﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface ILotService
    {
        Task<IEnumerable<Lot>> GetAllAsync();

        Task<Lot?> GetLotOrNullAsync(Guid id);

        Task<Guid> CreateAsync(LotCreateDto dto);

        Task UpdateAsync(Guid id, LotUpdateDto dto);

        Task DeleteAsync(Guid id);
    }
}
