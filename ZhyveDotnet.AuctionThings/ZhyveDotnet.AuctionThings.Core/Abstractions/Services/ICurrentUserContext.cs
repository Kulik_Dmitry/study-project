﻿using System;

namespace ZhyveDotnet.AuctionThings.Core.Abstractions.Services
{
    public interface ICurrentUserContext
    {
        Guid UserId { get; }

        string Name { get; }

        string Email { get; }
    }
}