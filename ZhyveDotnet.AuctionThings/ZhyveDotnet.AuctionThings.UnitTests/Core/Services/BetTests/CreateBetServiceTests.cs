﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Xunit;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.UnitTests.Builders.LotBuilder;

namespace ZhyveDotnet.AuctionThings.UnitTests.Core.Services.BetTests
{
    public class CreateBetServiceTests
    {
        private Mock<IBetRepository> _betRepositoryMock;
        private Mock<ILotRepository> _lotRepositoryMock;
        private Mock<IWinningStrategy> _winningStrategy;
        private BetService _betServiceMock;

        public CreateBetServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _betRepositoryMock = fixture.Freeze<Mock<IBetRepository>>();
            _lotRepositoryMock = fixture.Freeze<Mock<ILotRepository>>();
            _winningStrategy = fixture.Freeze<Mock<IWinningStrategy>>();
            _betServiceMock = fixture.Build<BetService>().Create();
        }

        [Fact]
        public async Task CreateBetAsync_BiddingIsClosed_WrongBetExceptionAsync()
        {
            Guid userId = Guid.NewGuid();
            Lot lot = new LotBuilder().WithInitialCost(4000).ForUser(userId).Build();
            var bets = new List<Bet>() { new Bet(userId, lot.Id, 6000), new Bet(userId, lot.Id, 8000) };
            bets.Last().AssingAsWinner();
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lot.Id)).ReturnsAsync(bets);

            Func<Task> act = async () => await _betServiceMock.CreateBetAsync(userId, lot.Id, 12000);
            await act.Should().ThrowAsync<WrongBetException>("Ошибка операции - невозможно совершить ставку, так как торги уже были закрыты.");
        }

        [Fact]
        public async Task CreateBetAsync_BetDoesNotExceedPreviousOne_WrongBetExceptionAsync()
        {
            Guid userId = Guid.NewGuid();
            Lot lot = new LotBuilder().WithInitialCost(4000).ForUser(userId).Build();
            var bets = new List<Bet>() { new Bet(userId, lot.Id, 6000), new Bet(userId, lot.Id, 8000) };
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lot.Id)).ReturnsAsync(bets);
            _winningStrategy.Setup(r => r.Determine(bets)).Returns(bets.Last());

            Func<Task> act = async () => await _betServiceMock.CreateBetAsync(userId, lot.Id, 7000);
            await act.Should().ThrowAsync<WrongBetException>($"Ошибка операции - невозможно совершить ставку, " +
                    $"так как новая ставка не превышает размер текущей.");
        }

        [Fact]
        public async Task CreateBetAsyn_CreateBet_BetCreatedAsync()
        {
            Guid userId = Guid.NewGuid();
            Lot lot = new LotBuilder().WithInitialCost(4000).ForUser(userId).Build();
            var bets = new List<Bet>() { new Bet(userId, lot.Id, 6000), new Bet(userId, lot.Id, 8000) };
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lot.Id)).ReturnsAsync(bets);
            _winningStrategy.Setup(r => r.Determine(bets)).Returns(bets.Last());

            var act = await _betServiceMock.CreateBetAsync(userId, lot.Id, 12000);
            act.Should().NotBeNull();
            act.LotId.Should().Be(lot.Id);
        }
    }
}
