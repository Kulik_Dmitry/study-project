﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Xunit;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.UnitTests.Builders.LotBuilder;

namespace ZhyveDotnet.AuctionThings.UnitTests.Core.Services.BetTests
{
    public class SetBetWinnerForLotServiceTests
    {
        private Mock<IBetRepository> _betRepositoryMock;
        private Mock<ILotRepository> _lotRepositoryMock;
        private Mock<IWinningStrategy> _winningStrategy;
        private BetService _betServiceMock;

        public SetBetWinnerForLotServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _betRepositoryMock = fixture.Freeze<Mock<IBetRepository>>();
            _lotRepositoryMock = fixture.Freeze<Mock<ILotRepository>>();
            _winningStrategy = fixture.Freeze<Mock<IWinningStrategy>>();
            _betServiceMock = fixture.Build<BetService>().Create();
        }

        [Fact]
        public async Task SetBetWinnerForLotAsync_LotNotFound_EntityNotFoundExceptionAsync()
        {
            Func<Task> act = async () => await _betServiceMock.SetBetWinnerForLotAsync(Guid.Empty);
            await act.Should().ThrowAsync<EntityNotFoundException>($"Сущность с id {Guid.Empty} не найдена");
        }

        [Fact]
        public async Task SetBetWinnerForLotAsync_BiddingIsClosed_InvalidOperationExceptionAsync()
        {
            Guid userId = Guid.NewGuid();
            Lot lot = new LotBuilder().WithInitialCost(4000).ForUser(userId).Build();
            var bets = new List<Bet>() { new Bet(userId, lot.Id, 6000), new Bet(userId, lot.Id, 8000) };
            bets.Last().AssingAsWinner();
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lot.Id)).ReturnsAsync(bets);

            Func<Task> act = async () => await _betServiceMock.SetBetWinnerForLotAsync(lot.Id);
            await act.Should().ThrowAsync<InvalidOperationException>("Ошибка операции - торги уже были закрыты.");
        }

        [Fact]
        public async Task SetBetWinnerForLotAsync_WinnerAssing_WinnerAssinedAsync()
        {
            Guid userId = Guid.NewGuid();
            Lot lot = new LotBuilder().WithInitialCost(4000).ForUser(userId).Build();
            var bets = new List<Bet>() { new Bet(userId, lot.Id, 6000), new Bet(userId, lot.Id, 8000) };
            _lotRepositoryMock.Setup(r => r.GetByIdAsync(lot.Id)).ReturnsAsync(lot);
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lot.Id)).ReturnsAsync(bets);
            _winningStrategy.Setup(r => r.Determine(bets)).Returns(bets.Last());

            await _betServiceMock.SetBetWinnerForLotAsync(lot.Id);
            bets.Last().IsWinner.Should().BeTrue();
        }
    }
}
