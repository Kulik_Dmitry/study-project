﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Xunit;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.UnitTests.Core.Services.BetTests
{
    public class BetServiceTests
    {
        private Mock<IBetRepository> _betRepositoryMock;
        private BetService _betServiceMock;

        public BetServiceTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _betRepositoryMock = fixture.Freeze<Mock<IBetRepository>>();
            _betServiceMock = fixture.Build<BetService>().Create();
        }

        [Fact]
        public async Task GetBetWinnerForLotOrNullAsync_LotDoesNotHaveWinnerBet_ReturnNullAsync()
        {
            Guid userId = Guid.NewGuid();
            Guid lotId = Guid.NewGuid();
            var bets = new List<Bet>() { new Bet(userId, lotId, 6000), new Bet(userId, lotId, 8000) };
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lotId)).ReturnsAsync(bets);

            var bet = await _betServiceMock.GetBetWinnerForLotOrNullAsync(lotId);
            bet.Should().BeNull();
        }

        [Fact]
        public async Task GetBetWinnerForLotOrNullAsync_LotHaveWinnerBet_ReturnBetWinnerAsync()
        {
            Guid userId = Guid.NewGuid();
            Guid lotId = Guid.NewGuid();
            var bets = new List<Bet>() { new Bet(userId, lotId, 6000), new Bet(userId, lotId, 8000) };
            bets.Last().AssingAsWinner();
            _betRepositoryMock.Setup(r => r.GetBetsByLotIdAsync(lotId)).ReturnsAsync(bets);

            var bet = await _betServiceMock.GetBetWinnerForLotOrNullAsync(lotId);
            bet.Should().NotBeNull();
            bet.IsWinner.Should().BeTrue();
        }
    }
}
