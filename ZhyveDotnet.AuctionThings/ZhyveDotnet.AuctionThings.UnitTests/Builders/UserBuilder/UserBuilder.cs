﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.User;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.UnitTests.Builders.UserBuilder
{
    public class UserBuilder
    {
        private User _user;

        public UserBuilder()
        {
            _user = new User(new UserCreateDto() { Login = "Dmitry", Password = "12qwasZX", Phone = "+375447788778" });
        }

        public User Build()
        {
            return _user;
        }
    }
}
