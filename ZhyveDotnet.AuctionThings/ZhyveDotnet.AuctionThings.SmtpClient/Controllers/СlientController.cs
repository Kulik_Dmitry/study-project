﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ZhyveDotnet.AuctionThings.SmtpClient.Core.Abstractions;
using ZhyveDotnet.AuctionThingsSmtpClient.DTOs;

namespace ZhyveDotnet.AuctionThings.SmtpClient.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class СlientController : Controller
    {
        private readonly IEmailService _emailService;

        private readonly IConfiguration _сonfiguration;

        public СlientController(IEmailService emailService, IConfiguration сonfiguration)
        {
            _emailService = emailService;
            _сonfiguration = сonfiguration;
        }

        [HttpPost("Send")]
        public async Task<ActionResult> SendAsync(EmailMessageDto message)
        {
            await _emailService.SendAsync(message.Recipient, message.Subject, message.Body);
            return Ok();
        }
    }
}
