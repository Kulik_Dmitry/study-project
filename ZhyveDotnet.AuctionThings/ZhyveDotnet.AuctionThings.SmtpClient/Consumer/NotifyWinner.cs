﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using ZhyveDotnet.AuctionThings.Core.Contracts;
using ZhyveDotnet.AuctionThings.Core.Domain.Contracts;
using ZhyveDotnet.AuctionThings.SmtpClient.Core.Abstractions;

namespace ZhyveDotnet.AuctionThings.SmtpClient.Consumer
{
    public class NotifyWinner : IConsumer<IWinnerNotification>
    {
        private IEmailService _service;

        public NotifyWinner(IEmailService service)
        {
            _service = service;
        }

#pragma warning disable UseAsyncSuffix // Use Async suffix
        public async Task Consume(ConsumeContext<IWinnerNotification> context)
#pragma warning restore UseAsyncSuffix // Use Async suffix
        {
            await _service.SendAsync(context.Message.Email, context.Message.Subject, context.Message.Body);

            await context.RespondAsync<EmptyResponse>(new EmptyResponse());
        }
    }
}
