﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services.Files;
using ZhyveDotnet.AuctionThings.Core.Domain.Files;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class FileController : ControllerBase
    {
        private readonly ICurrentUserContext _currentUserContext;
        private readonly IFileDocumentService _fileDocumentService;

        public FileController(ICurrentUserContext currentUserContext, IFileDocumentService fileDocumentService)
        {
            _currentUserContext = currentUserContext;
            _fileDocumentService = fileDocumentService;
        }

        [HttpPost]
        public async Task<FileDocument> CreateAsync(IFormFile file)
        {
            await using var fileStream = file.OpenReadStream();
            return await _fileDocumentService
                .CreateAsync(HttpUtility.HtmlDecode(file.FileName), fileStream, _currentUserContext.UserId)
                .ConfigureAwait(false);
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<FileDocument> GetFileAsync(Guid id)
        {
            return await _fileDocumentService.GetFileAsync(id).ConfigureAwait(false);
        }

        [HttpGet]
        public async Task<IReadOnlyList<FileDocument>> GetFileCollectionAsync()
        {
            return await _fileDocumentService.GetFileCollectionAsync(filter: null).ConfigureAwait(false);
        }

        [HttpGet]
        [Route("Content/{id:guid}")]
        public async Task<ActionResult> GetFileContentAsync(Guid id, string privateKey)
        {
            var file = await _fileDocumentService.GetFileAsync(id).ConfigureAwait(false);
            if (file == null)
            {
                return NotFound();
            }

            var fileStream = await _fileDocumentService.GetFileContentAsync(file, privateKey).ConfigureAwait(false);
            if (fileStream == null)
            {
                return Forbid();
            }

            return File(fileStream, "application/octet-stream", file.PublicName);
        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<ActionResult> UpdateAsync(Guid id, IFormFile file)
        {
            await using var fileStream = file.OpenReadStream();
            await _fileDocumentService
                .UpdateAsync(id, HttpUtility.HtmlDecode(file.FileName), fileStream)
                .ConfigureAwait(false);
            return Ok();
        }
    }
}