﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto.Comment;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        /// <summary>
        /// Добавление нового комментария.
        /// </summary>
        /// <param name="comment">Комментарий.</param>
        /// <returns>Статус 200.</returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync(CommentDto comment)
        {
            await _commentService.CreateAsync(comment);
            return Ok();
        }

        /// <summary>
        /// Получить список комментариев для лота.
        /// </summary>
        /// <param name="lotId">Идентификатор лота.</param>
        /// <returns>Список комментариев.</returns>
        [HttpGet("{lotId:guid}")]
        public async Task<IEnumerable<Comment>> GetCommentsByLotIdAsync(Guid lotId)
        {
            var comments = await _commentService.GetAllByLotIdAsync(lotId);
            return comments;
        }

        /// <summary>
        /// Удалить комментарий.
        /// </summary>
        /// <param name="id">Идентификатор комментария.</param>
        /// <returns>Статус 200.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            await _commentService.DeleteAsync(id);
            return Ok();
        }

        /// <summary>
        /// Обновить комментарий.
        /// </summary>
        /// <param name="id">Идентификатор комментария.</param>
        /// <param name="content">Комментарий.</param>
        /// <returns>Статус 200.</returns>
        [HttpPut]
        public async Task<ActionResult> UpdateAsync(Guid id, string content)
        {
            await _commentService.UpdateAsync(id, content);
            return Ok();
        }
    }
}
