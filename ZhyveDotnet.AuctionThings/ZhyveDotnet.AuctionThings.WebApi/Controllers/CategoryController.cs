﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Dto;
using ZhyveDotnet.AuctionThings.Core.Application.Exceptions;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Создание новой категории.
        /// </summary>
        /// <param name="categoryDto">Новая категория.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPost]
        public async Task<ActionResult<Category>> CreateCategoryAsync(CategoryDto categoryDto)
        {
            var category = await _categoryService.CreateAsync(categoryDto);
            return Ok(category);
        }

        /// <summary>
        /// Получить все категории.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet]
        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            return await _categoryService.GetAllAsync();
        }

        /// <summary>
        /// Получить данные о категории.
        /// </summary>
        /// <param name="id">Id категории.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Category?>> GetCategoryAsync(Guid id)
        {
            var category = await _categoryService.GetCategoryOrNullAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(category);
            }
        }

        /// <summary>
        /// Удалить категорию.
        /// </summary>
        /// <param name="id">Id категории.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> RemoveСategoryAsync(Guid id)
        {
              await _categoryService.DeleteAsync(id);
              return Ok();
        }

        /// <summary>
        /// Обновить информацию о категории.
        /// </summary>
        /// <param name="id">Id обновляемой категории.</param>
        /// <param name="categoryDto">Данные обновленной категории.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPut]
        public async Task<ActionResult> EditCategoryAsync(Guid id, CategoryDto categoryDto)
        {
            await _categoryService.UpdateAsync(id, categoryDto);
            return Ok();
        }
    }
}
