﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BetController : Controller
    {
        private readonly IBetService _betService;
        private readonly ICurrentUserContext _currentUserContext;

        public BetController(IBetService betService, ICurrentUserContext currentUserContext)
        {
            _betService = betService;
            _currentUserContext = currentUserContext;
        }

        /// <summary>
        /// Получить все ставки на лот.
        /// </summary>
        /// <param name="lotId">Id лота.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpGet("{lotId:guid}")]
        public async Task<ActionResult<IEnumerable<Bet>>> GetBetsByLotIdAsync(Guid lotId)
        {
            if (lotId == default)
            {
                return BadRequest("Отстутствует id лота.");
            }

            var bets = await _betService.GetBetsByLotIdAsync(lotId);
            return Ok(bets);
        }

        /// <summary>
        /// Получить мои ставки на все лоты.
        /// </summary>
        /// <returns>Список ставок.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bet>>> GetMyBetsAsync()
        {
            var bets = await _betService.GetBetsByUserIdAsync(_currentUserContext.UserId).ConfigureAwait(false);
            return Ok(bets);
        }

        /// <summary>
        /// Совершить ставку на лот.
        /// </summary>
        /// <param name="userId">Id пользователя, совершающего ставку.</param>
        /// <param name="lotId">Id лота, на который совершают ставку.</param>
        /// <param name="amount">Размер ставки.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [HttpPost]
        public async Task<ActionResult> MakeBetAsync(Guid userId, Guid lotId, decimal amount)
        {
            await _betService.CreateBetAsync(userId, lotId, amount);
            return Ok();
        }
    }
}