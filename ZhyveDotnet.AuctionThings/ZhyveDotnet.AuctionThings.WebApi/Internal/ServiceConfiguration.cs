﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Repositories.Files;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services;
using ZhyveDotnet.AuctionThings.Core.Abstractions.Services.Files;
using ZhyveDotnet.AuctionThings.Core.Application.Services;
using ZhyveDotnet.AuctionThings.Core.Application.Services.Comment;
using ZhyveDotnet.AuctionThings.Core.Application.Services.Files;
using ZhyveDotnet.AuctionThings.Core.Application.Services.Role;
using ZhyveDotnet.AuctionThings.Core.Application.Validators;
using ZhyveDotnet.AuctionThings.Core.Domain;
using ZhyveDotnet.AuctionThings.DataAccess.Repositories;
using ZhyveDotnet.AuctionThings.DataAccess.Repositories.Files;

namespace ZhyveDotnet.AuctionThings.WebApi.Internal
{
    public static class ServiceConfiguration
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddServices(configuration)
                .AddDataRepositories(configuration)
                .AddValidators(configuration)
                .AddScoped<ICurrentUserContext, CurrentUserContext>();

            return services;
        }

        private static IServiceCollection AddValidators(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IValidator<Lot>, LotValidator>();
            services.AddTransient<IValidator<Bet>, BetValidator>();
            return services;
        }

        private static IServiceCollection AddDataRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IFileDocumentRepository, FileDocumentRepository>();
            services.AddScoped<ILotRepository, LotRepository>();
            services.AddScoped<ILotStateRepository, LotStateRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IBetRepository, BetRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            return services;
        }

        private static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ILotService, LotService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ILotStateService, LotStateService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IBetService, BetService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddFileService(configuration);
            return services;
        }

        private static IServiceCollection AddFileService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IFileDocumentService, FileDocumentService>();

            var fileDocumentSettings =
                configuration.GetSection(nameof(FileDocumentSettings)).Get<FileDocumentSettings>() ??
                new FileDocumentSettings();

            services.AddSingleton(fileDocumentSettings);

            return services;
        }
    }
}