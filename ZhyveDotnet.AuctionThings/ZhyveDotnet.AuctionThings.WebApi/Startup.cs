﻿using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using ZhyveDotnet.AuctionThings.Core.Application.Providers;
using ZhyveDotnet.AuctionThings.Core.Application.WinningStrategies;
using ZhyveDotnet.AuctionThings.Core.Domain.Contracts;
using ZhyveDotnet.AuctionThings.Core.Domain.Providers;
using ZhyveDotnet.AuctionThings.DataAccess;
using ZhyveDotnet.AuctionThings.WebApi.Internal;
using ZhyveDotnet.AuctionThings.WebApi.Middlewares;

namespace ZhyveDotnet.AuctionThings.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddDbContext<DataContext>(x => { x.UseNpgsql(Configuration.GetConnectionString("TestDb")); });

            // https://vaibhavbhapkarblogs.medium.com/jwt-authentication-authorization-in-net-core-3-1-e762a7abe00a
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer("Bearer", options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.Authority = Configuration.GetValue<string>("Authority");
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateLifetime = true,
                        ValidateAudience = false
                    };
                });

            services.AddCoreServices(Configuration);
            services.AddScoped<IWinningStrategy, BaseWinningStratery>();

            services.AddMassTransit(config =>
            {
                config.AddBus(context => Bus.Factory.CreateUsingRabbitMq(c =>
                {
                    c.UseJsonSerializer();

                    c.Host("localhost", "/", h =>
                    {
                        h.Username("guest");
                        h.Password("guest");
                    });
                    c.ConfigureEndpoints(context);
                }));

                config.AddRequestClient<IWinnerNotification>();
            });

            services.AddMassTransitHostedService();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });

            services.AddTransient<IDateTimeProvider, DateTimeProvider>();

            services.AddCors();

            // Для получения HttpContext (точнее IHttpContextAccessor) в конструкторе сервисов / контроллеров
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseExceptionHandlingMiddleware();

            if (env.IsDevelopment())
            {
                dbInitializer.InitializeDb();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder =>
            {
                builder
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins("http://localhost:3000");
            });

            app.UseOpenApi();
            app.UseSwaggerUi3(x => { x.DocExpansion = "list"; });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseActionLogMiddleware();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}