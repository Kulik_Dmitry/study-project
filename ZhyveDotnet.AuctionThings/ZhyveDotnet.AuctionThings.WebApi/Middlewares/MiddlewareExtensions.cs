﻿using Microsoft.AspNetCore.Builder;

namespace ZhyveDotnet.AuctionThings.WebApi.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }

        public static IApplicationBuilder UseActionLogMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ActionLogMiddleware>();
        }
    }
}