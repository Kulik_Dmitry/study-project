﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace ZhyveDotnet.AuctionThings.WebApi.Middlewares
{
    public class ActionLogMiddleware
    {
        private const string SeparatorItem = "\r\n";

        private readonly RequestDelegate _next;

        public ActionLogMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, ILogger<ActionLogMiddleware> logger)
        {
            var start = Stopwatch.GetTimestamp();

            if (httpContext?.Request?.ContentType == "application/json")
            {
                httpContext.Request.EnableBuffering();
            }

            try
            {
                await _next(httpContext).ConfigureAwait(false);

                var statusCode = httpContext?.Response?.StatusCode;
                var level = statusCode >= 500 ? LogLevel.Error : LogLevel.Information;

                await LogRequestAsync(
                    logger,
                    level,
                    httpContext,
                    statusCode,
                    GetElapsedMilliseconds(start, Stopwatch.GetTimestamp()));
            }
            catch (Exception ex) when (
                LogException(logger, httpContext, ex, GetElapsedMilliseconds(start, Stopwatch.GetTimestamp())))
            {
            }
        }

        private bool LogException(ILogger logger, HttpContext? httpContext, Exception ex, double elapsedMilliseconds)
        {
            LogRequestAsync(logger, LogLevel.Error, httpContext, 500, elapsedMilliseconds).GetAwaiter().GetResult();

            return false;
        }

        private async Task LogRequestAsync(
            ILogger logger, LogLevel logLevel, HttpContext? httpContext, int? statusCode, double elapsedMilliseconds)
        {
            if (httpContext?.Request != null)
            {
                if (HttpMethods.IsPost(httpContext.Request.Method) ||
                    HttpMethods.IsPut(httpContext.Request.Method))
                {
                    if (httpContext.Request.HasFormContentType)
                    {
                        GetLogger(
                                logger,
                                httpContext,
                                statusCode,
                                elapsedMilliseconds,
                                ("Files", GetStringFromFiles(httpContext.Request.Form.Files)),
                                ("Form", GetStringFromItems(httpContext.Request.Form)))
                            .Log(logLevel, "Input {@form} request", "form");

                        return;
                    }
                    else if (httpContext.Request.ContentType == "application/json")
                    {
                        // httpContext.Request.EnableBuffering();
                        httpContext.Request.Body.Seek(0, SeekOrigin.Begin);

                        using var reader = new StreamReader(
                            httpContext.Request.Body,
                            encoding: System.Text.Encoding.UTF8,
                            detectEncodingFromByteOrderMarks: false,
                            bufferSize: 1024,
                            leaveOpen: true);

                        GetLogger(logger, httpContext, statusCode, elapsedMilliseconds)
                            .Log(logLevel, await reader.ReadToEndAsync());

                        return;
                    }
                }
                else if (HttpMethods.IsGet(httpContext.Request.Method) ||
                         HttpMethods.IsDelete(httpContext.Request.Method))
                {
                    GetLogger(logger, httpContext, statusCode, elapsedMilliseconds)
                        .Log(logLevel, "Input {@query} request", "query");

                    return;
                }

                GetLogger(logger, httpContext, statusCode, elapsedMilliseconds)
                    .Log(logLevel, "Simple request");
            }
        }

        private ILogger GetLogger(
            ILogger logger,
            HttpContext httpContext,
            int? statusCode,
            double elapsedMilliseconds,
            params (string key, object value)[] addList)
        {
            var list = new List<(string key, object value)>
            {
                ("ConnectionId", httpContext.Connection.Id),
                ("TraceId", httpContext.TraceIdentifier),
                ("Protocol", httpContext.Request.Protocol),
                ("HTTP", httpContext.Request.Method),
                ("Url", $"{httpContext.Request.Path}{httpContext.Request.QueryString}"),
                ("Query", GetStringFromItems(httpContext.Request.Query)),
                ("Headers", GetStringFromItems(httpContext.Request.Headers)),
                ("UserInfo", httpContext.User.Identity?.Name ?? string.Empty)
            };

            if (addList?.Any() == true)
            {
                list.AddRange(addList);
            }

            list.Add(("Response", $"{statusCode} in {elapsedMilliseconds:0.0000} ms"));

            return logger.WithProperties(list.ToArray());
        }

        private string GetStringFromItems(
            IEnumerable<KeyValuePair<string, Microsoft.Extensions.Primitives.StringValues>> items)
        {
            return string.Join(
                SeparatorItem,
                items
                    .Where(x =>
                        !string.IsNullOrEmpty(x.Value) &&
                        x.Key != "password")
                    .Select(x =>
                        $"{x.Key}: {x.Value.ToString().Replace("\r", string.Empty).Replace("\n", string.Empty)}"));
        }

        private string GetStringFromFiles(IEnumerable<IFormFile> files)
        {
            return string.Join(
                SeparatorItem,
                files
                    .Where(x => x != null)
                    .Select(x => $"{x.Name}: {x.FileName} [{x.Length:N2} B]"));
        }

        private double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }
    }
}