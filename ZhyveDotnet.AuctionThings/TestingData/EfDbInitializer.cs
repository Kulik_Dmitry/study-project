﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ZhyveDotnet.AuctionThings.Core.Domain;

namespace ZhyveDotnet.AuctionThings.DataAccess
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
         {
            _dataContext.Database.Migrate();

            if (!_dataContext.Users.Any())
            {
                _dataContext.AddRange(FakeDataFactory.Users);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Lots);
                _dataContext.SaveChanges();

                _dataContext.LotStates.AddRange(FakeDataFactory.LotStates);
                _dataContext.SaveChanges();

                _dataContext.Bet.AddRange(FakeDataFactory.Bets);
                _dataContext.SaveChanges();

                _dataContext.UpdateRange(FakeDataFactory.Users);
                _dataContext.SaveChanges();

                _dataContext.UpdateRange(FakeDataFactory.Bets);
                _dataContext.SaveChanges();
            }
        }
    }
}