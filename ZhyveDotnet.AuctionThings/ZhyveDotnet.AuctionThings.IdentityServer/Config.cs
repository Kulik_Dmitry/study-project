﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                 new ApiScope("api1", "Otus API")
            };

        public static IEnumerable<Client> Clients(string frontUrl) =>
             new List<Client>
            {
                // Javascript client
                new Client
                {
                    ClientId = "js_oidc",
                    ClientName = "Javascript Client",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("hehehe")
                    },
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    AbsoluteRefreshTokenLifetime = 200,
                    IdentityTokenLifetime = 15,
                    AccessTokenLifetime = 100,
                    AuthorizationCodeLifetime = 15,
                    SlidingRefreshTokenLifetime = 120,

                    RedirectUris =
                    {
                        $"{frontUrl}/callback.html",
                        $"{frontUrl}/silent.html"
                    },
                    PostLogoutRedirectUris = { $"{frontUrl}/index.html" },
                    AllowedCorsOrigins = { $"{frontUrl}" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "api1"
                    },

                    AlwaysIncludeUserClaimsInIdToken = true
                }
            };
    }
}