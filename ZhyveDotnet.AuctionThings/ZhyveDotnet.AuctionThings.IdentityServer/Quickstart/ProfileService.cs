﻿using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;

namespace IdentityServer.Quickstart
{
    public class ProfileService
        : IProfileService
    {
        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();

            var user = TestUsers.Users.FirstOrDefault(x => x.SubjectId == sub);
            if (user != null)
            {
                context.IssuedClaims = user.Claims
                    .Where(x => context.RequestedClaimTypes.Contains(x.Type))
                    .ToList();
            }

            return Task.CompletedTask;
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();

            var user = TestUsers.Users.FirstOrDefault(x => x.SubjectId == sub);
            context.IsActive = user != null;

            return Task.CompletedTask;
        }
    }
}